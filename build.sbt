// Used to get a nice project name in Intellij IDEA
name := Settings.name

lazy val root = project.in(file(".")).
  aggregate(js, jvm).
  settings(
    // Disable publishing of the root project
    publish      := {},
    publishLocal := {}
  )

lazy val cross = crossProject.in(file("."))
  .settings(
    name           := Settings.name,
    normalizedName := Settings.normalizedName,
    version        := Settings.version,
    organization   := Settings.organization,
    scalaVersion   := Settings.versions.scala,
    scalacOptions  ++= Seq(
      "-deprecation",            // Emit warning and location for usages of deprecated APIs.
      "-feature",                // Emit warning and location for usages of features that should be imported explicitly.
      "-unchecked",              // Enable additional warnings where generated code depends on assumptions.
      "-Ywarn-inaccessible",     // Warn about inaccessible types in method signatures.
      "-Ywarn-nullary-override"  // Warn when non-nullary overrides nullary, e.g. def foo() over def foo.
      //"-Ywarn-dead-code",        // Warn when dead code is identified.
      //"-Xfatal-warnings",        // Fail the compilation if there are any warnings.
      //"-Xlint",                  // Enable recommended additional warnings.
      //"-Ywarn-adapted-args",     // Warn if an argument list is modified to match the receiver.
      //"-Ywarn-numeric-widen"     // Warn when numerics are widened.
    ),

    homepage       := Settings.homepage,
    licenses       += Settings.licenses,
    scmInfo        := Settings.scmInfo,

    publishTo <<= version { Settings.publishTo() },
    publishMavenStyle := true,
    publishArtifact in Test := false,

    pomExtra             := Settings.pomExtra,
    pomIncludeRepository := { _ => false },

    libraryDependencies ++= Settings.sharedDependencies.value
  )
  .settings(Settings.resolverSettings: _*)
  .jvmSettings(
    libraryDependencies ++= Settings.jvmDependencies.value
  )
  .jsSettings(
    libraryDependencies ++= Settings.scalajsDependencies.value,
    jsDependencies      ++= Settings.jsDependencies.value,
    jsDependencies      +=  RuntimeDOM % "test"
  )

lazy val jvm = cross.jvm
lazy val js  = cross.js
