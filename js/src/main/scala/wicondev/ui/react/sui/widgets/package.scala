package wicondev.ui.react.sui

package object widgets {
  trait all  
    extends wicondev.ui.generic.sui.all
    with    Accordion
    with    Checkbox
    with    Dropdown
    with    Menu
    with    MenuContainer
    with    MultipleSearchSelect
    with    OptionalRadioGroup
    with    Panel
    with    RadioGroup
    with    Select
    with    TagsInput
    with    TypedEditor

  object all extends all
}
