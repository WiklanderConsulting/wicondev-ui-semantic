package wicondev.ui.react

package object sui {
  trait all
    extends wicondev.ui.react.all
    with    wicondev.ui.generic.sui.all
    with    wicondev.ui.react.sui.widgets.all

    with    BreadCrumbs
    with    Button
    with    Form
    with    Grid
    with    Icon
    with    Loader
    with    Message

  object all extends all {
    import scala.language.implicitConversions
    import wicondev.ui.generic.sui.SuiClassSet

    @inline implicit final def suiClassSet2TagMod(sui: SuiClassSet): TagMod  = ^.cls := sui.set mkString " "

    def s = SuiClassSet()
  }
}
