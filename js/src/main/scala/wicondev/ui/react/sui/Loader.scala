package wicondev.ui.react.sui

import japgolly.scalajs.react.ReactElement
import wicondev.ui.react.sui.all._

trait Loader {
  object Loader {
    def apply(): ReactElement =
      <.div(s.ui.segment)(
        <.p(),
        <.div(s.ui.active.inverted.dimmer)(
          <.div(s.ui.loader)
        ),
        <.img(s.ui.centered.image, ^.src:="/assets/img/paragraph.png")
      )
  }
}
