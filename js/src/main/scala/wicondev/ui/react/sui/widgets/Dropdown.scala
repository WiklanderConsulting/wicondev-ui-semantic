package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react._
import org.scalajs.jquery.jQuery
import wicondev.ui.generic.sui.SuiClassSet
import wicondev.ui.react.sui.all._
import wicondev.ui.react.sui.modules.Dropdown._
import wicondev.ui.react.widgets.{OptionalItemSelector, ItemSelector}

import scala.scalajs.js
import scala.scalajs.js.Dynamic.literal

// TODO: Create a DropdownBase and refactor common functionality between DropdownSelector and OptionalDropdownSelector

trait Dropdown {
  trait DropdownSelector[ItemType] extends ItemSelector[ItemType] {
    def dropdownClass: SuiClassSet = s.ui.dropdown

    override def render = $ => {
      <.div(dropdownClass, $.props.hasError ?= s.error)(
        <.div(s.text)($.props.asLegend($.props.selected)),
        Icon(IconType.DROPDOWN),
        <.div(s.menu)(
          $.props.items map (item => <.div(s.item, data("value", $.props.asValue(item)))($.props.asLegend(item)))
        )
      )
    }

    override protected def componentDidMount = $ => Callback {
      val onShow: Unit = {
        $.props.onFocus.runNow()
      }

      val onHide: js.UndefOr[js.Any] => Unit = (maybeValue: js.UndefOr[js.Any]) => {
        maybeValue map(value => $.props.onBlur($.props.itemMap(value.toString)).runNow())
      }

      val onChange: js.UndefOr[js.Any] => Unit = (maybeValue: js.UndefOr[js.Any]) => {
        maybeValue map(value => $.props.onChange($.props.itemMap(value.toString)).runNow())
      }

      jQuery($.getDOMNode()).dropdown(literal(
        "onShow"   -> onShow,
        "onHide"   -> onHide,
        "onChange" -> onChange
      ))
    }

    override def componentDidUpdate = update => Callback(
      jQuery(update.$.getDOMNode())
        .dropdown(REFRESH_DROPDOWN)
    )
  }

  trait OptionalDropdownSelector[ItemType] extends OptionalItemSelector[ItemType] {
    def dropdownClass: SuiClassSet = s.ui.dropdown

    override def render = $ => {
      <.div(dropdownClass, $.props.hasError ?= s.error)(
        <.div(s.text)($.props.asLegend($.props.selected)),
        Icon(IconType.DROPDOWN),
        <.div(s.menu)(
          $.props.items map (item => <.div(s.item, data("value", $.props.asValue(Some(item))))($.props.asLegend(Some(item))))
        )
      )
    }

    override protected def componentDidMount = $ => Callback {
      def onShow(): Unit =
        $.props.onFocus.runNow()

      def onHide(maybeValue: js.UndefOr[js.Any]): Unit =
        $.props.onBlur(maybeValue.toOption flatMap(value => $.props.itemMap.get(value.toString))).runNow()

      def onChange(maybeValue: js.UndefOr[js.Any]): Unit =
        maybeValue map(value => $.props.onChange($.props.itemMap.get(value.toString)).runNow())

      jQuery($.getDOMNode()).dropdown(literal(
        "onShow"   -> onShow _,
        "onHide"   -> onHide _,
        "onChange" -> onChange _
      ))
    }

    override def componentDidUpdate = update => Callback(
      jQuery(update.$.getDOMNode())
        .dropdown(REFRESH_DROPDOWN)
    )
  }

  trait DropdownSelection[ItemType]              extends DropdownSelector[ItemType] { override val dropdownClass = s.ui.selection.dropdown }
  trait DropdownButton[ItemType]                 extends DropdownSelector[ItemType] { override val dropdownClass = s.ui.dropdown.button }
  trait DropdownCompactSelectionButton[ItemType] extends DropdownSelector[ItemType] { override val dropdownClass = s.ui.compact.selection.dropdown.button }

  trait OptionalDropdownSelection[ItemType]      extends OptionalDropdownSelector[ItemType] { override val dropdownClass = s.ui.selection.dropdown }
}
