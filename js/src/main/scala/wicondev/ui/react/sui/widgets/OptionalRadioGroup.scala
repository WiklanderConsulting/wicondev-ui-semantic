package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react._
import wicondev.ui.generic.sui.SuiClassSet
import wicondev.ui.react.sui.all._
import wicondev.ui.react.widgets.OptionalItemSelector

trait OptionalRadioGroup {
  trait OptionalRadioGroup[ItemType] extends OptionalItemSelector[ItemType] {

    protected def fieldClass: SuiClassSet = s.grouped.fields

    override def render = $ => {
      def onChangeIfChecked(maybeItem: Option[ItemType]): ReactEventI => Option[Callback] = event => {
        if(event.target.checked) Some($.props.onChange(maybeItem)) else None
      }

      def radioElementFor(maybeItem: Option[ItemType]) =
        <.div(
          s.ui.radio.checkbox,
          maybeItem == $.props.selected ?= s.checked
        )(
          <.input(
            ^.tpe      := InputType.RADIO.asString,
            ^.value    := $.props.asValue(maybeItem),
            ^.checked  := maybeItem == $.props.selected,
            $.props.id map(id => ^.id := id),
            ^.onChange ==>? onChangeIfChecked(maybeItem)
          ),

          <.label($.props.asLegend(maybeItem))
        )

      <.div(fieldClass)(
        $.props.label map (<.label(_)),

        <.div(s.field)(
          radioElementFor(None)
        ),

        $.props.items map {item =>
          <.div(s.field)(
            radioElementFor(Some(item))
          )
        }
      )
    }
  }
}
