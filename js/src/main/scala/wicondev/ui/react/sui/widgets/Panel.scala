package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react.ReactElement
import wicondev.ui.react.sui.all._

trait Panel {
  object Panel {
    def apply(f: ReactElement*): ReactElement =
      <.div(s.ui.segment)(f)
    
    def WithHeader(
      panelType: PanelType,
      headerElement: ReactElement
    )(f: ReactElement*): Seq[ReactElement] = Seq(
      <.div(s.ui.top.attached.message.ofType(panelType))(headerElement),
      <.div(s.ui.bottom.attached.segment)(f)
    )

    def WithHeaderAndBody(
      panelType: PanelType,
      headerElement: ReactElement
    )(f: ReactElement*): Seq[ReactElement] =
      (
        <.div(s.ui.top.attached.message.ofType(panelType))(headerElement): ReactElement
      ) +: f

    def WithTitle(
      panelType: PanelType,
      title: String
    )(f: ReactElement*): Seq[ReactElement] = Seq(
      <.div(s.ui.top.attached.message.ofType(panelType))(
        <.div(s.header)(title)
      ),
      <.div(s.ui.bottom.attached.segment)(f)
    )

    def WithTitle(
      panelType: PanelType,
      titleReactElement: ReactElement
    )(f: ReactElement*): Seq[ReactElement] = Seq(
      <.div(s.ui.top.attached.message.ofType(panelType))(titleReactElement),
      <.div(s.ui.bottom.attached.segment)(f)
    )

    def Title(title: String) = <.h5(s.ui.header)(title)

    def WithHeaderAndFooter(
      panelType: PanelType,
      headerElement: ReactElement,
      footerElement: ReactElement
    )(f: ReactElement*): Seq[ReactElement] = Seq(
      <.div(s.ui.top.attached.message.ofType(panelType))(headerElement),
      <.div(s.ui.attached.segment)(f),
      <.div(s.ui.bottom.attached.message.ofType(panelType))(footerElement)
    )

    def WithFooter(panelType: PanelType)(f: ReactElement*)(footerReactElement: ReactElement): Seq[ReactElement] = Seq(
      <.div(s.ui.top.attached.segment)(f),
      <.div(s.ui.bottom.attached.message.ofType(panelType))(footerReactElement)
    )
  }
}
