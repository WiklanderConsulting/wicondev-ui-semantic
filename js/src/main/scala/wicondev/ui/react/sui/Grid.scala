package wicondev.ui.react.sui

import japgolly.scalajs.react.ReactElement

import wicondev.ui.react.sui.all._

trait Grid {
  object Grid {
    def apply(f: ReactElement*): ReactElement =
      <.div(s.ui.grid)(f)

    def Centered(f: ReactElement*): ReactElement =
      <.div(s.ui.centered.grid)(f)
  }

  def Row(f: ReactElement*): ReactElement =
    <.div(s.row)(f)

  def Row(customClass: String)(f: ReactElement*): ReactElement =
    <.div(s.row.withClass(customClass))(f)

  def Column(f: ReactElement*): ReactElement = Column(ColumnSize.NONE)(f: _*)

  def Column(customClass: String, f: ReactElement*): ReactElement = Column(size = ColumnSize.NONE, customClass = customClass)(f: _*)

  def Column(
    size:       ColumnSize  = ColumnSize.NONE,
    hAlignment: Alignment   = Alignment.NONE,
    vAlignment: Alignment   = Alignment.NONE,
    float: Float            = Float.NONE,
    customClass: String     = ""
  )(f: ReactElement*): ReactElement =
    <.div(
      classSetS(
        hAlignment.asString,
        vAlignment.asString,
        float.asString,
        if(size isNot ColumnSize.NONE) s"$size ${s.wide}" else "",
        s.column,
        customClass
      )
    )(f)
}
