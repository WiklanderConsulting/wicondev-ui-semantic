package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react.Callback
import wicondev.ui.generic.sui.SuiClassSet
import wicondev.ui.react.sui.all._
import wicondev.ui.react.widgets.StatefulWidget

trait Menu {
  case class MenuProps(
    items:      Seq[MenuItem],
    activeItem: Option[MenuItem] = None,
    onChange:   MenuItem => Callback
  )
  
  case class MenuItem(
    title:   String,
    icon:    Option[IconType] = None
  )
  
  object IconMenuItem {
    def apply(title: String, icon: IconType) = MenuItem(title, Some(icon))
  }
  
  trait MenuWidget extends StatefulWidget[MenuProps, Option[MenuItem]] {
    override protected def initialState = P => P.activeItem
  
    override protected def render = $ => {
      renderMenu(
        $.props.items map (item =>
          renderItem(item, $.state contains item, $)
        )
      )
    }
  
    protected def menuClass: SuiClassSet
  
    protected def renderMenu(items: Seq[ReactTag]) = <.div(menuClass)(items)
  
    protected def renderItem(item: MenuItem, active: Boolean = false, $: Scope) = {
      (if(active) {
        <.a(s.active.item)
      } else {
        <.a(
          s.item,
          ^.onClick       --> setActive(item, $),
          ^.onTouchStart  --> setActive(item, $)
        )
      })(
        item.icon map (Icon(_)),
        item.title
      )
    }
  
    protected def setActive(item: MenuItem, $: Scope): Callback =
      $.setState(Some(item)) >> $.props.onChange(item)
  }
  
  object Menu                    extends MenuWidget {override def menuClass = s.ui.menu}
  object VerticalMenu            extends MenuWidget {override def menuClass = s.ui.vertical.menu}
  object TabularMenu             extends MenuWidget {override def menuClass = s.ui.tabular.menu}
  object IconMenu                extends MenuWidget {override def menuClass = s.ui.icon.menu}
  object LabeledIconMenu         extends MenuWidget {override def menuClass = s.ui.labeled.icon.menu}
  object VerticalIconMenu        extends MenuWidget {override def menuClass = s.ui.vertical.icon.menu}
  object VerticalLabeledIconMenu extends MenuWidget {override def menuClass = s.ui.vertical.labeled.icon.menu}
}
