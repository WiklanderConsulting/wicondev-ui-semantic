package wicondev.ui.react.sui

import japgolly.scalajs.react.ReactElement
import wicondev.ui.generic.sui.SuiClassSet
import wicondev.ui.react.sui.all._

trait Form {
  object Form {
    def apply(content: ReactElement*): ReactElement = makeForm(
      s.ui.form
    )(content: _*)

    def apply(
      formClasses: String*
    )(content: ReactElement*): ReactElement = makeForm(
      s.ui.form.withClasses(formClasses: _*)
    )(content: _*)

    def large(
      formClasses: String*
    )(content: ReactElement*): ReactElement = makeForm(
      s.ui.large.form.withClasses(formClasses: _*)
    )(content: _*)

    def small(
      formClasses: String*
    )(content: ReactElement*): ReactElement = makeForm(
      s.ui.small.form.withClasses(formClasses: _*)
    )(content: _*)
    
    private def makeForm(
      formClasses: SuiClassSet
    )(content: ReactElement*): ReactElement = {
      <.form(
        formClasses,
        ^.role := "form"
      )(content)
    }
  }
}
