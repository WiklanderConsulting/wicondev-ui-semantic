package wicondev.ui.react.sui.widgets

import wicondev.data.utils.TimeUtils
import japgolly.scalajs.react._
import org.scalajs.dom.html.Div
import wicondev.data.units.{Percentage, Timestamp}
import wicondev.ui.react.sui.all._
import wicondev.ui.react.widgets.StatefulWidget

trait TypedEditor {
  trait TypedEditorTypes[Type] {
    type MaybeCallback  = Option[Callback]
    type TypeToCallback = Option[Type] => MaybeCallback
  }

  trait TypedEditorProps[Type] extends TypedEditorTypes[Type] {
    def placeholder: Option[String]
    def value:       Option[Type]
    def disabled:    Boolean
    def saving:      Boolean
    def onSubmit:    TypeToCallback
  }

  case class TypedEditorState[Type](
    item:       Option[Type],
    editedItem: Option[Type],
    active:     Boolean = false,
    selected:   Boolean = false
  ) extends TypedEditorTypes[Type] {
    def hasChanged = editedItem != item
  }

  trait HasEditorTags {
    protected def fieldClass: String
    protected def extraFieldClass: String = ""

    protected def contentWrapperTag: ReactTag =
      <.span(^.className := "editable-field-content")
  }

  trait TypedEditor[T, Props <: TypedEditorProps[T]]
  extends StatefulWidget[Props, TypedEditorState[T]]
  with HasEditorTags
  with TypedEditorTypes[T] {
    type Type = T

    val fieldRef = Ref[Div]("editableField")
    val fieldKey = "editableField"

    override protected def initialState = P => TypedEditorState(P.value, P.value)

    override protected def componentWillReceiveProps = $ => {
      val valueMod: Callback =
        if ($.$.state.active) {
          if ($.nextProps.value != $.$.state.item) {
            $.$.setState(TypedEditorState(
              item       = $.nextProps.value,
              editedItem = $.currentState.editedItem,
              active     = $.currentState.active,
              selected   = $.currentState.selected
            ))
          } else {
            Callback.empty
          }
        } else {
          $.$.modState(_.copy(item = $.nextProps.value, editedItem = $.nextProps.value))
        }

      $.$.modState(_.copy(active = $.nextProps.saving)) >> valueMod
    }

    protected def handleChange(implicit $: Scope): TypeToCallback =
      (value: Option[Type]) => Option($.modState(_.copy(editedItem = value)))

    protected def handleSubmit(implicit $: Scope): Callback =
    // Only run submit actions if the content has changed
      if ($.state.editedItem == $.state.item) {
        deactivate
      } else {
        $.props.onSubmit($.state.editedItem) getOrElse handleCancel
      }

    /**
      * Override this for validation
      */
    protected def canSubmit(implicit $: Scope): Boolean = true

    protected def handleCancel(implicit $: Scope): Callback =
      $.modState(_.copy(editedItem = $.state.item)) >>
      deactivate

    protected def activate(implicit $: Scope): Callback =
      $.modState(_.copy(active = true)) >>
      deselect

    protected def deactivate(implicit $: Scope): Callback =
      deselect >>
      $.modState(_.copy(active = false))

    protected def select(implicit $: Scope): Callback =
      $.modState(_.copy(selected = true))

    protected def deselect(implicit $: Scope): Callback =
      $.modState(_.copy(selected = false))

     override def render = $ => {
      implicit val scope: Scope = $

      if($.state.active) {
        fieldTag($)(
          renderActive,
          maybeNot($.props.saving)(
            renderControls
          )
        )
      } else {
        fieldTag($)(^.title := "Click to edit")(
          renderPassive
        )
      }
    }

    protected def renderPassive(implicit $: Scope): ReactElement =
      contentWrapperTagWithHandlers($)(
        $.state.editedItem.map (value =>
          contentTag(value)
        ) getOrElse <.i(s.disabled)($.props.placeholder): ReactElement,

        passiveOverlayIcon
      )

    protected def renderActive(implicit $: Scope): ReactElement =
      <.div(^.className := "inline-edit-fields")(
        inputElement,
        activeOverlayIcon
      )

    protected def inputElement(implicit $: Scope): ReactElement

    protected def activeOverlayIcon: ReactElement =
      <.span(^.className := "overlay-icon")(Icon(IconType("notched circle loading")))

    protected def passiveOverlayIcon: ReactElement =
      <.span(^.className := "overlay-icon")(Icon(IconType.WRITE))

    protected def renderControls(implicit $: Scope): ReactElement =
      <.div(^.className := "save-options")(
        <.button(
          s.ui.mini.positive.icon.button,
          ^.title     := "Save",
          ^.onClick  --> handleSubmit,
          ^.disabled  := !canSubmit
        )(Icon(IconType.CHECKMARK)),
        <.button(
          s.ui.mini.negative.icon.button,
          ^.title    := "Cancel",
          ^.onClick --> handleCancel
        )(Icon(IconType.REMOVE))
      )


    protected def contentWrapperTagWithHandlers(implicit $: Scope): ReactTag =
      contentWrapperTag(
        ^.onClick     --> activate,
        ^.onMouseOver --> select,
        ^.onMouseOut  --> deselect
      )

    protected def fieldTag(implicit $: Scope): ReactTag = <.div(
      ^.classSet1(
        fieldClass,
        extraFieldClass -> extraFieldClass.nonEmpty,
        "selected"      -> $.state.selected,
        "active"        -> $.state.active,
        "saving"        -> $.props.saving
      ),
      ^.ref := fieldRef,
      ^.key := fieldKey
    )

    protected def contentTag(value: T)(implicit $: Scope): ReactElement
  }

  trait HasFluidTags extends HasEditorTags {
    override protected val extraFieldClass = "fluid"

    override protected def contentWrapperTag: ReactTag =
      <.div(^.className := "editable-content")
  }

  trait TypedFormEditor[T, Props <: TypedEditorProps[T]] extends TypedEditor[T, Props] with HasFluidTags {
    override protected val fieldClass = "editable-form"
  }

  trait TypedTableEditor[T, Props <: TypedEditorProps[T]] extends TypedEditor[T, Props] with HasFluidTags {
    override protected val fieldClass = "editable-table"
  }

  trait TypedFieldEditor[T, Props <: TypedEditorProps[T]] extends TypedEditor[T, Props] {
    override protected val fieldClass = "editable-field"
  }

  trait FluidTypedFieldEditor[T, Props <: TypedEditorProps[T]] extends TypedFieldEditor[T, Props] with HasFluidTags

  trait TypedInputEditor[T, Props <: TypedEditorProps[T]] extends TypedEditor[T, Props] {
    override protected val fieldClass = "editable-input"
  }

  trait FluidTypedInputEditor[T, Props <: TypedEditorProps[T]] extends TypedInputEditor[T, Props] with HasFluidTags

  trait StringEditor extends TypedInputEditor[String, TypedEditorProps[String]] {
    case class Props(
      placeholder: Option[String] = None,
      value:       Option[Type]   = None,
      disabled:    Boolean        = false,
      saving:      Boolean        = false,
      onSubmit:    TypeToCallback = _ => None
    ) extends TypedEditorProps[Type]

    override protected def contentTag(value: Type)(implicit $: Scope): ReactElement =
      <.span(value)

    override protected def inputElement(implicit $: Scope) = {
      <.div(s.ui.fluid.input)(
        TextInput(TextInput.Props(
          value       = $.state.editedItem,
          onChange    = handleChange,
          onSubmit    = value => Option(handleSubmit),
          onEscape    = value => Option(handleCancel)
        ))
      )
    }
  }

  trait FluidStringEditor extends StringEditor with HasFluidTags

  object TextEditor extends StringEditor

  object TransparentTextEditor extends StringEditor {
    override protected val extraFieldClass = "transparent"
  }

  object FluidTextEditor extends FluidStringEditor

  object MultiLineTextEditor extends FluidStringEditor {
    override protected val extraFieldClass = "fluid multiline"

    override protected def contentTag(value: Type)(implicit $: Scope): ReactElement =
      <.div(value.split("\n").flatMap(line => Seq[TagMod](line, <.br())))

    override protected def inputElement(implicit $: Scope) = {
      <.div(s.ui.fluid.input)(
        MultiLineTextInput(TextInput.Props(
          value       = $.state.editedItem,
          onChange    = handleChange,
          onSubmit    = value => Option(handleSubmit),
          onEscape    = value => Option(handleCancel)
        ))
      )
    }
  }

  object DateEditor extends TypedInputEditor[Timestamp, TypedEditorProps[Timestamp]] {
    case class Props(
      placeholder: Option[String] = None,
      value:       Option[Type]   = None,
      disabled:    Boolean        = false,
      saving:      Boolean        = false,
      onSubmit:    TypeToCallback = _ => None
    ) extends TypedEditorProps[Type]

    override protected def contentTag(value: Type)(implicit $: Scope): ReactElement =
      <.span(TimeUtils.formatIsoDate(value))

    override protected def inputElement(implicit $: Scope) = {
      <.div(s.ui.fluid.input)(
        DateInput(DateInput.Props(
          value    = $.state.editedItem,
          onChange = handleChange,
          onSubmit = value => Option(handleSubmit),
          onEscape = value => Option(handleCancel)
        ))
      )
    }
  }

  object PercentageEditor extends TypedInputEditor[Percentage, TypedEditorProps[Percentage]] {
    case class Props(
      placeholder: Option[String] = None,
      value:       Option[Type]   = None,
      disabled:    Boolean        = false,
      saving:      Boolean        = false,
      onSubmit:    TypeToCallback = _ => None
    ) extends TypedEditorProps[Type]

    override protected def contentTag(value: Type)(implicit $: Scope): ReactElement =
      <.span(value.toString, "%")

    override protected def inputElement(implicit $: Scope) = {
      <.div(s.ui.fluid.input)(
        PercentageInput(PercentageInput.Props(
          value    = $.state.editedItem,
          onChange = handleChange,
          onSubmit = _ => Option(handleSubmit),
          onEscape = _ => Option(handleCancel)
        ))
      )
    }
  }
}
