package wicondev.ui.react.sui

import japgolly.scalajs.react.ReactElement
import wicondev.ui.react.sui.all._

trait BreadCrumbs {
  case class BreadCrumb(title: String, href: String)

  object BreadCrumbs {
    def apply(items: BreadCrumb*) = <.div(s.ui.breadcrumb)(build(items))
    def small(items: BreadCrumb*) = <.div(s.ui.small.breadcrumb)(build(items))
    def large(items: BreadCrumb*) = <.div(s.ui.large.breadcrumb)(build(items))
    def huge(items:  BreadCrumb*) = <.div(s.ui.huge.breadcrumb)(build(items))

    private def build(items: Seq[BreadCrumb]): Seq[ReactElement] = {
      def divider: ReactElement = <.div(s.divider)(Icon(IconType.CHEVRON_RIGHT))
      def renderItem(      item: BreadCrumb): ReactElement = <.a(s.section, ^.href := item.href)(item.title)
      def renderActiveItem(item: BreadCrumb): ReactElement = <.div(s.active.section)(item.title)

      // One or zero items
      if(items.size < 2) {
        items map renderActiveItem
      } else {
        val breadCrumbs: Seq[ReactElement] = items map renderItem

        // Drop the last divider and item and add an active item at the end of the list
        breadCrumbs
          .flatMap(breadCrumb => Seq(breadCrumb, divider))
          .reverse
          .drop(2)
          .reverse :+ renderActiveItem(items.reverse.head)
      }
    }
  }
}
