package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react.Callback
import org.scalajs.jquery.{JQuery, jQuery}
import wicondev.ui.logger.Loggable
import wicondev.ui.react.sui.all._
import wicondev.ui.react.sui.modules.Dropdown._
import wicondev.ui.react.widgets.Widget

import scala.scalajs.js
import scala.scalajs.js.Dynamic.literal

trait TagsInput {
  @deprecated("Move to MultipleSearchSelect with allowAdditions = true, instead", "0.3.0")
  trait TagsInput[ItemType] extends Widget[TagsInputProps[ItemType]] with Loggable {
    override def render = $ => {
      <.div(s.field)(
        $.props.label map (<.label(_)),
        <.div(s.ui.fluid.multiple.search.selection.dropdown)(
          <.input($.props.id map(id => ^.id := id), ^.tpe:="hidden", ^.value:=$.props.selected.map($.props.asValue).mkString(",")),
          Icon(IconType.DROPDOWN),
          $.props.placeholder map (<.div(s.default.text)(_)),

          <.div(s.menu)(
            $.props.selected map (item =>
              <.div(s.item, data("value", $.props.asValue(item)))($.props.asLegend(item))
            )
          )
        )
      )
    }

    def dropdownNode(scope: ScopeM): JQuery = jQuery(scope.getDOMNode()).find(".dropdown")

    def dropdown(scope: ScopeM, args: js.Object): JQuery = dropdownNode(scope).dropdown(args)

    def dropdown(scope: ScopeM, behaviour: String, args: js.Any*): JQuery = dropdownNode(scope).dropdown(behaviour, args)

    def initDropdown($: ScopeM): Unit = {
      val onAdd: (js.UndefOr[js.Any], js.UndefOr[js.Any], js.UndefOr[js.Any]) => Unit = (value: js.UndefOr[js.Any], text: js.UndefOr[js.Any], $addedChoice: js.UndefOr[js.Any]) => {
        text map(text => $.props.onAdd(text.toString).runNow())
      }

      val onRemove: (js.UndefOr[js.Any], js.UndefOr[js.Any], js.UndefOr[js.Any]) => Unit = (value: js.UndefOr[js.Any], text: js.UndefOr[js.Any], $addedChoice: js.UndefOr[js.Any]) => {
        value map(value => $.props.onRemove($.props.itemMap(value.toString)).runNow())
      }

      dropdown($, literal(
        "allowAdditions" -> true,
        "onAdd"          -> onAdd,
        "onRemove"       -> onRemove
      ))
    }

    override protected def componentDidMount = $ => Callback(initDropdown($))

    override protected def componentDidUpdate = update => Callback(initDropdown(update.$))
  }

  case class TagsInputProps[ItemType](
    items:        Seq[ItemType],
    selected:     Seq[ItemType],
    onAdd:        String => Callback,
    onRemove:     ItemType => Callback,
    id:           Option[String]     = None,
    asValue:      ItemType => String = (item: ItemType) => item.toString,
    asLegend:     ItemType => String = (item: ItemType) => item.toString,
    label:        Option[String] = None,
    placeholder:  Option[String] = None
  ) {
    val itemMap: Map[String, ItemType] = Map(items map (item => asValue(item) -> item): _*)
  }
}
