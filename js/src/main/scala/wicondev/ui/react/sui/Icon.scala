package wicondev.ui.react.sui

import japgolly.scalajs.react.ReactElement
import japgolly.scalajs.react._
import wicondev.ui.generic.sui.classes
import wicondev.ui.react.sui.all._

trait Icon {
  object Icon {
    def apply(
      iconType:       IconType,
      iconSize:       IconSize          = IconSize.NORMAL,
      iconFlip:       IconFlip          = IconFlip.NONE,
      iconRotatation: IconRotation      = IconRotation.NONE,
      iconTag:        ReactTag          = <.i,
      onClick:        Option[Callback]  = None
    ): ReactElement =
      iconTag(
        typedClassSet1(
          classes.icon,
          iconType,
          iconFlip,
          iconRotatation,
          iconSize
        ),
        ^.onClick -->? onClick
        // Icon need *some* content to render
      )(" ")

    def linked(
      iconType:       IconType,
      iconSize:       IconSize          = IconSize.NORMAL,
      iconFlip:       IconFlip          = IconFlip.NONE,
      iconRotatation: IconRotation      = IconRotation.NONE,
      iconTag:        ReactTag          = <.i,
      onClick:        Option[Callback]  = None
    ) = apply(
      iconType       = iconType,
      iconSize       = iconSize,
      iconFlip       = iconFlip,
      iconRotatation = iconRotatation,
      iconTag        = iconTag(^.cls := classes.link),
      onClick        = onClick
    )
  }
}
