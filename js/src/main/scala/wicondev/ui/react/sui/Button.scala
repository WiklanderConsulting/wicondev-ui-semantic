package wicondev.ui.react.sui

import japgolly.scalajs.react.{Callback, ReactElement}
import wicondev.ui.react.sui.all._

trait Button {
  object Button {
    def apply(
      label: String,
      onClick: Callback
    ) =
      <.div(
        s.ui.button,
        ^.onClick --> onClick
      )(label)

    def saveOrCancel(
      onSave:   Callback,
      onCancel: Callback
    ): ReactElement = {
      <.div(s.ui.buttons)(
        <.div(
          s.ui.positive.button,
          ^.onClick --> onSave
        )("Save"),

        <.div(s.or),

        <.div(
          s.ui.button,
          ^.onClick --> onCancel
        )("Cancel")
      )
    }

    def saveOrCancelInline(
      onSave:   Callback,
      onCancel: Callback
    ): ReactElement = {
      <.div(s.ui.buttons)(
        <.div(
          s.ui.positive.button,
          ^.onClick --> onSave
        )("Save"),

        <.div(s.or),

        <.div(
          s.ui.button,
          ^.onClick --> onCancel
        )("Cancel")
      )
    }
  }
}
