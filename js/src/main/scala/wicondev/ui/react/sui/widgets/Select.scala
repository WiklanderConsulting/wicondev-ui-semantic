package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react._
import wicondev.ui.react.sui.all._
import wicondev.ui.react.widgets.ItemSelector

trait Select {
  trait Select[ItemType] extends ItemSelector[ItemType] {
    override def render = $ => {
      <.div(s.field)(
        $.props.label map (<.label($.props.id map(id => ^.htmlFor := id))(_)),

        <.select(
          s.dropdown,
          $.props.id map(id => ^.id := id),
          ^.value    := $.props.asValue($.props.selected),
          ^.onChange ==> { event: ReactEventI => $.props.onChange($.props.itemMap(event.target.value)) }
        )(
          $.props.items map {item =>
            <.option(^.value  := $.props.asValue(item))($.props.asLegend(item))
          }
        )
      )
    }
  }
}
