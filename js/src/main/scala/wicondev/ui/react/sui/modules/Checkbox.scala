package wicondev.ui.react.sui.modules

import org.scalajs.jquery.JQuery

import scala.language.implicitConversions
import scala.scalajs.js

@js.native
trait Checkbox extends JQuery {
  def checkbox(args: js.Any*): Any = js.native
}

object Checkbox {
  implicit def jq2checkbox($: JQuery): Checkbox =
    $.asInstanceOf[Checkbox]

  implicit class CheckboxOps(val self: Checkbox) extends AnyVal {
    def checkbox(): Checkbox = self.checkbox().asInstanceOf[Checkbox]

    /**
      * Switches a checkbox from current state
      */
    def toggle(): Unit                    = self.checkbox(Checkbox.TOGGLE)

    /**
      * Set a checkbox state to checked
      */
    def check(): Unit                     = self.checkbox(Checkbox.CHECK)

    /**
      * Set a checkbox state to unchecked
      */
    def uncheck(): Unit                   = self.checkbox(Checkbox.UNCHECK)

    /**
      * Set as indeterminate checkbox
      */
    def indeterminate(): Unit             = self.checkbox(Checkbox.INDETERMINATE)

    /**
      * Set as determinate checkbox
      */
    def determinate(): Unit               = self.checkbox(Checkbox.DETERMINATE)

    /**
      * Enable interaction with a checkbox
      */
    def enable(): Unit                    = self.checkbox(Checkbox.ENABLE)

    /**
      * Set a checkbox state to checked without callbacks
      */
    def setChecked(): Unit                = self.checkbox(Checkbox.SET_CHECKED)

    /**
      * Set a checkbox state to unchecked without callbacks
      */
    def setUnchecked(): Unit              = self.checkbox(Checkbox.SET_UNCHECKED)

    /**
      * Set as indeterminate checkbox without callbacks
      */
    def setIndeterminate(): Unit          = self.checkbox(Checkbox.SET_INDETERMINATE)

    /**
      * Set as determinate checkbox without callbacks
      */
    def setDeterminate(): Unit            = self.checkbox(Checkbox.SET_DETERMINATE)

    /**
      * Enable interaction with a checkbox without callbacks
      */
    def setEnabled(): Unit                = self.checkbox(Checkbox.SET_ENABLED)

    /**
      * Disable interaction with a checkbox without callbacks
      */
    def setDisabled(): Unit               = self.checkbox(Checkbox.SET_DISABLED)

    /**
      * Returns whether element is radio selection
      */
    def isRadio: Boolean                  = self.checkbox(Checkbox.IS_RADIO).asInstanceOf[Boolean]

    /**
      * Returns whether element is currently checked
      */
    def isChecked: Boolean                = self.checkbox(Checkbox.IS_CHECKED).asInstanceOf[Boolean]

    /**
      * Returns whether element is not checked
      */
    def isUnchecked: Boolean              = self.checkbox(Checkbox.IS_UNCHECKED).asInstanceOf[Boolean]

    /**
      * Returns whether element is able to be changed
      */
    def canChange: Boolean                = self.checkbox(Checkbox.CAN_CHANGE).asInstanceOf[Boolean]

    /**
      * Returns whether element can be checked (checking if already checked or `beforeChecked` would cancel)
      */
    def shouldAllowCheck: Boolean         = self.checkbox(Checkbox.SHOULD_ALLOW_CHECK).asInstanceOf[Boolean]

    /**
      * Returns whether element can be unchecked (checking if already unchecked or `beforeUnchecked` would cancel)
      */
    def shouldAllowUncheck: Boolean       = self.checkbox(Checkbox.SHOULD_ALLOW_UNCHECK).asInstanceOf[Boolean]

    /**
      * Returns whether element can be determinate (checking if already determinate or `beforeDeterminate` would cancel)
      */
    def shouldAllowDeterminate: Boolean   = self.checkbox(Checkbox.SHOULD_ALLOW_DETERMINATE).asInstanceOf[Boolean]

    /**
      * Returns whether element can be indeterminate (checking if already indeterminate or `beforeIndeterminate` would cancel)
      */
    def shouldAllowIndeterminate: Boolean = self.checkbox(Checkbox.SHOULD_ALLOW_INDETERMINATE).asInstanceOf[Boolean]

    /**
      * Returns whether element is able to be unchecked
      */
    def canUncheck: Boolean               = self.checkbox(Checkbox.CAN_UNCHECK).asInstanceOf[Boolean]
  }

  val TOGGLE                     = "toggle"
  val CHECK                      = "check"
  val UNCHECK                    = "uncheck"
  val INDETERMINATE              = "indeterminate"
  val DETERMINATE                = "determinate"
  val ENABLE                     = "enable"
  val SET_CHECKED                = "set checked"
  val SET_UNCHECKED              = "set unchecked"
  val SET_INDETERMINATE          = "set indeterminate"
  val SET_DETERMINATE            = "set determinate"
  val SET_ENABLED                = "set enabled"
  val SET_DISABLED               = "set disabled"
  val IS_RADIO                   = "is radio"
  val IS_CHECKED                 = "is checked"
  val IS_UNCHECKED               = "is unchecked"
  val CAN_CHANGE                 = "can change"
  val SHOULD_ALLOW_CHECK         = "should allow check"
  val SHOULD_ALLOW_UNCHECK       = "should allow uncheck"
  val SHOULD_ALLOW_DETERMINATE   = "should allow determinate"
  val SHOULD_ALLOW_INDETERMINATE = "should allow indeterminate"
  val CAN_UNCHECK                = "can uncheck"
}
