package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react._
import wicondev.ui.react.sui.all._
import wicondev.ui.react.widgets.ItemSelector

trait RadioGroup {
  trait RadioGroup[ItemType] extends ItemSelector[ItemType] {

    override def render = $ => {
      def onChangeIfChecked(item: ItemType): ReactEventI => Option[Callback] = event => {
        if(event.target.checked) Some($.props.onChange(item)) else None
      }

      <.div(s.grouped.fields)(
        $.props.label map (<.label(_)),

        $.props.items map {item =>
          <.div(s.field)(
            <.div(
              s.ui.radio.checkbox,
              item == $.props.selected ?= s.checked
            )(
                <.input(
                  ^.tpe      := InputType.RADIO.asString,
                  ^.value    := $.props.asValue(item),
                  ^.checked  := item == $.props.selected,
                  $.props.id map(id => ^.id := id),
                  ^.onChange ==>? onChangeIfChecked(item)
                ),
                <.label($.props.asLegend(item))
              )
          )
        }
      )
    }
  }
}
