package wicondev.ui.react.sui.modules

import org.scalajs.jquery.JQuery

import scala.language.implicitConversions
import scala.scalajs.js

@js.native
trait Dropdown extends JQuery {
  def dropdown(args: js.Any*): this.type = js.native
}

object Dropdown {
  val REFRESH_DROPDOWN: String = "refresh"

  implicit def jq2dropdown($: JQuery): Dropdown =
    $.asInstanceOf[Dropdown]
}
