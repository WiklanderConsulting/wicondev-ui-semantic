package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react._
import org.scalajs.jquery.jQuery
import wicondev.ui.react.sui.all._
import wicondev.ui.react.sui.modules.Accordion._
import wicondev.ui.react.widgets.Widget

import scala.scalajs.js.Dynamic.literal

trait Accordion {
  case class AccordionItem(
    title:   ReactElement,
    content: ReactElement*
  )

  object Accordion extends Widget[Seq[AccordionItem]] {
    override def render = $ => <.div(s.ui.fluid.styled.accordion)(
      $.props.flatMap { item => Seq(
        <.div(s.title)(
          Icon(IconType.DROPDOWN),
          item.title
        ),

        <.div(s.content)(item.content)
      )}
    )

    override protected def componentDidMount = $ => Callback(
      jQuery($.getDOMNode()).accordion(literal(
        "duration"        -> 150,
        "animateChildren" -> false,
        "selector"        -> literal(
          "trigger" -> ".dropdown.icon"
        )
      ))
    )

    override def componentDidUpdate = update => Callback(
      jQuery(update.$.getDOMNode())
        .accordion(REFRESH_ACCORDION)
    )
  }
}
