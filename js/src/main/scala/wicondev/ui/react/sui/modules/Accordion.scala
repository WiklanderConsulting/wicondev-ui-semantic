package wicondev.ui.react.sui.modules

import org.scalajs.jquery.JQuery

import scala.language.implicitConversions
import scala.scalajs.js

@js.native
trait Accordion extends JQuery {
  def accordion(args: js.Any*): this.type = js.native
}

object Accordion {
  val REFRESH_ACCORDION: String = "refresh"

  implicit def jq2accordion($: JQuery): Accordion =
    $.asInstanceOf[Accordion]
}
