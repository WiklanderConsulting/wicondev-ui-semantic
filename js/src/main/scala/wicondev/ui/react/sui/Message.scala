package wicondev.ui.react.sui

import japgolly.scalajs.react.ReactElement
import wicondev.ui.react.sui.all._

trait Message {
  object Message {
    def apply(
      messageType: MessageType = MessageType.INFO,
      messageSize: MessageSize = MessageSize.NORMAL,
      iconType:    Option[IconType] = None

    )(f: ReactElement*): ReactElement =
      makeMessage(
        messageType = messageType,
        messageSize = messageSize,
        maybeIcon   = iconType,
        dismissable = false
      )(f: _*)

    def Dismissable(
      messageType: MessageType = MessageType.INFO,
      messageSize: MessageSize = MessageSize.NORMAL,
      iconType:    Option[IconType] = None
    )(f: ReactElement*): ReactElement =
      makeMessage(
        messageType = messageType,
        messageSize = messageSize,
        maybeIcon   = iconType,
        dismissable = true
      )(f: _*)

    private def makeMessage(
      messageType: MessageType,
      messageSize: MessageSize,
      maybeIcon:   Option[IconType],
      dismissable: Boolean
    )(f: ReactElement*): ReactElement = {
      val maybeDismiss = maybe(dismissable)(Icon(IconType.CLOSE))

      maybeIcon map (iconType => <.div(
        s.ui.icon.message.withSize(messageSize).ofType(messageType),
        ^.role:="alert"
      )(
        maybeDismiss,
        <.i(s.icon.ofType(iconType)),
        <.div(s.content)(f)
      ): ReactElement) getOrElse <.div(
        s.ui.message.withSize(messageSize).ofType(messageType),
        ^.role := "alert"
      )(
        maybeDismiss,
        f
      )
    }
  }
}
