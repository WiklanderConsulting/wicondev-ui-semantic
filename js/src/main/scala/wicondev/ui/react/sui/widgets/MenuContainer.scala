package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react.ReactElement
import wicondev.ui.react.sui.all._
import wicondev.ui.react.widgets.StatefulWidget

trait MenuContainer {
  case class MenuPair(
    menuItem: MenuItem,
    content:  ReactElement
  )

  case class MenuContainerProps(
    menuPairs:   Seq[MenuPair],
    activePair:  Seq[MenuPair] => MenuPair = _.head,
    menuType:    MenuWidget = Menu
  )

  object MenuContainer extends StatefulWidget[MenuContainerProps, MenuItem] {
    override protected def initialState = P => P.activePair(P.menuPairs).menuItem

    override protected def render = $ => {
      def isActivePair(pair: MenuPair): Boolean = pair.menuItem == $.state

      <.div(^.className := "menu-container")(
        $.props.menuType(MenuProps(
          items      = $.props.menuPairs map (_.menuItem),
          activeItem = Some($.state),
          onChange   = item => $.setState(item)
        )),

        $.props.menuPairs.zipWithIndex.map {case (menuPair, index) =>
          maybe(isActivePair(menuPair))(<.div(^.key := s"panel-$index")(menuPair.content))
        }
      )
    }
  }
}
