package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react.CallbackOption._
import japgolly.scalajs.react._
import org.scalajs.dom.ext.KeyCode
import wicondev.ui.generic.sui.SuiClassSet
import wicondev.ui.react.all._

import scala.util.{Failure, Try}

trait Checkbox {
  trait CheckboxBase[Type] extends TypedInputBase[Type] {
    case class Props(
      id:          Option[String]                   = None,
      ref:         Option[Ref]                      = None,
      autoFocus:   Option[Boolean]                  = None,
      placeholder: Option[String]                   = None,
      value:       MaybeType                        = None,
      disabled:    Boolean                          = false,
      readOnly:    Boolean                          = false,
      onFocus:     MaybeCallback                    = None,
      onChange:    TypeToCallback = (ot: MaybeType) => None,
      onBlur:      TypeToCallback = (ot: MaybeType) => None,
      onSubmit:    TypeToCallback = (ot: MaybeType) => None,
      onEscape:    TypeToCallback = (ot: MaybeType) => None
    ) extends TypedInputProps[Type]

    override val htmlType = InputType.CHECK_BOX

    protected def booleanFromType(t: Type): Boolean
    protected def booleanToType(b: Boolean): Type
    override protected def valueToType(v: String): Try[Type] =
      Failure(new IllegalStateException(s"""Can't convert value "$v" to Type. An input of type checkbox does not have a string value."""))

    def checkboxClass: SuiClassSet

    override protected def delegateEvent(
      event: ReactEventI,
      handler: TypeToCallback
    ): MaybeCallback = {
      handler(Some(booleanToType(event.target.checked)))
    }

    override protected def onKeyDown(implicit $: Scope): ReactKeyboardEventI => Callback =
      event => keyCodeSwitch(event) {
        case KeyCode.Space =>
          delegateEvent(event, $.props.onChange) map(_ >> event.preventDefaultCB) getOrElse Callback.empty
      } orElse invalidKeys(event)

    override protected def inputElement(implicit $: Scope): ReactTag =
      <.div(
        ^.classSet(
          checkboxClass.toString                     -> $.props.placeholder.isDefined,
          checkboxClass.withClass("fitted").toString -> $.props.placeholder.isEmpty
        )
      )(
        <.input(
          ^.tpe            := htmlType.asString,
          ^.id             := $.props.id orElse Some("checkbox"),
          ^.cls            := "hidden",
          ^.ref            := $.props.ref,
          ^.autoFocus      := $.props.autoFocus,
          ^.checked        := $.props.value map booleanFromType,
          $.props.disabled ?= (^.disabled := "disabled"),
          $.props.readOnly ?= (^.readOnly := "readOnly"),
          ^.placeholder    := $.props.placeholder,
          ^.onFocus        -->? $.props.onFocus,
          ^.onBlur         ==>? onBlur,
          ^.onKeyDown      ==> onKeyDown,
          ^.onChange       ==>? onChange
        ),

        <.label(!$.props.readOnly ?= (^.htmlFor := $.props.id getOrElse "checkbox"))($.props.placeholder)
      )
  }

  trait CheckBox[Type]       extends CheckboxBase[Type] { override val checkboxClass = SuiClassSet().ui.checkbox }
  trait ToggleCheckBox[Type] extends CheckboxBase[Type] { override val checkboxClass = SuiClassSet().ui.toggle.checkbox }
  trait SliderCheckBox[Type] extends CheckboxBase[Type] { override val checkboxClass = SuiClassSet().ui.slider.checkbox }

  trait BooleanCheckBox {
    protected def booleanFromType(t: Boolean): Boolean = t
    protected def booleanToType  (b: Boolean): Boolean = b
  }

  object CheckBox       extends CheckBox[Boolean] with BooleanCheckBox
  object ToggleCheckBox extends ToggleCheckBox[Boolean] with BooleanCheckBox
  object SliderCheckBox extends SliderCheckBox[Boolean] with BooleanCheckBox
}
