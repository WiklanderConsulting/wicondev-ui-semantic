package wicondev.ui.react.sui.widgets

import japgolly.scalajs.react.Callback
import org.scalajs.jquery.{JQuery, jQuery}
import wicondev.ui.logger.Loggable
import wicondev.ui.react.sui.all._
import wicondev.ui.react.sui.modules.Dropdown._
import wicondev.ui.react.widgets.{MultipleItemSelectorProps, Widget}

import scala.scalajs.js
import scala.scalajs.js.Dynamic.literal

trait MultipleSearchSelect {
  case class MultipleSearchSelectProps[ItemType](
    items:                  Seq[ItemType],
    selected:               Seq[ItemType],
    id:                     Option[String]       = None,
    label:                  Option[String]       = None,
    hasError:               Boolean              = false,
    placeholder:            Option[String]       = None,
    allowAdditions:         Boolean              = false,
    onAdd:                  String   => Callback = (value: String)  => Callback.empty,
    onSelect:               ItemType => Callback = (item: ItemType) => Callback.empty,
    onRemove:               ItemType => Callback = (item: ItemType) => Callback.empty,
    override val asValue:   ItemType => String   = (item: ItemType) => item.toString,
    override val asLegend:  ItemType => TagMod   = (item: ItemType) => item.toString
  ) extends MultipleItemSelectorProps[ItemType] {
    override def onChange: Seq[ItemType] => Callback = (items: Seq[ItemType]) => Callback.empty
  }

  trait MultipleSearchSelect[ItemType] extends Widget[MultipleSearchSelectProps[ItemType]] with Loggable {
    override def render = $ => {
      <.div(s.field)(
        $.props.label map (<.label(_)),
        <.div(s.ui.multiple.search.selection.dropdown)(
          <.input($.props.id map(id => ^.id := id), ^.tpe:="hidden", ^.value:=$.props.selected.map($.props.asValue).mkString(",")),
          Icon(IconType.DROPDOWN),
          $.props.placeholder map (<.div(s.default.text)(_)),

          <.div(s.menu)(
            $.props.items map (item =>
              <.div(s.item, data("value", $.props.asValue(item)))($.props.asLegend(item))
            )
          )
        )
      )
    }

    def dropdownNode(scope: ScopeM): JQuery = jQuery(scope.getDOMNode()).find(".dropdown")

    def dropdown(scope: ScopeM, args: js.Object): JQuery = dropdownNode(scope).dropdown(args)

    def dropdown(scope: ScopeM, behaviour: String, args: js.Any*): JQuery = dropdownNode(scope).dropdown(behaviour, args)

    def initDropdown(scope: ScopeM): Unit = {
      val onAdd: (js.UndefOr[js.Any], js.UndefOr[js.Any], js.UndefOr[js.Any]) => Unit = (value: js.UndefOr[js.Any], text: js.UndefOr[js.Any], $added: js.UndefOr[js.Any]) => {
        value foreach { value: js.Any =>
          (
            scope.props.itemMap.get(value.toString) map { item =>
              scope.props.onSelect(item)
            } getOrElse {
              scope.props.onAdd(text.get.toString)
            }
          ).runNow()
        }
      }

      val onRemove: (js.UndefOr[js.Any], js.UndefOr[js.Any], js.UndefOr[js.Any]) => Unit = (value: js.UndefOr[js.Any], text: js.UndefOr[js.Any], $removed: js.UndefOr[js.Any]) => {
        value foreach(value => scope.props.onRemove(scope.props.itemMap(value.toString)).runNow())
      }

      dropdown(scope, literal(
        "allowAdditions" -> scope.props.allowAdditions,
        "onAdd"          -> onAdd,
        "onRemove"       -> onRemove
      ))
    }

    override protected def componentDidMount = $ => Callback(
      initDropdown($)
    )

    override def componentDidUpdate = update => Callback(
      initDropdown(update.$)
    )
  }
}
