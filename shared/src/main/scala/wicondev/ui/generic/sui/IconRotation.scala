package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait IconRotation {
  case class IconRotation(value: String*) extends TypedClass(value: _*)

  object IconRotation {
    val NONE              = IconRotation("")
    val CLOCKWISE         = IconRotation(clockwise,        rotated)
    val COUNTER_CLOCKWISE = IconRotation(counterclockwise, rotated)
  }
}
