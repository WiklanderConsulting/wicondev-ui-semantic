package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait ButtonType {
  case class ButtonType(value: String) extends TypedClass(value)

  object ButtonType {
    val DEFAULT   = ButtonType("")
    val BASIC     = ButtonType(basic)
    val PRIMARY   = ButtonType(primary)
    val SECONDARY = ButtonType(secondary)
    val POSITIVE  = ButtonType(positive)
    val NEGATIVE  = ButtonType(negative)
  }
}
