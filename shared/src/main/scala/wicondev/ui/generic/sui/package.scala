package wicondev.ui.generic

package object sui {
  
  trait all
    extends Alignment
    with    ButtonType
    with    ColumnSize
    with    Float
    with    IconFlip
    with    IconRotation
    with    IconSize
    with    IconType
    with    MessageType
    with    MessageSize
    with    PanelType
    with    ProgressType
    with    Size {
  }

  object classes extends Classes
}
