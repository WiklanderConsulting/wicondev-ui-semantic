package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait IconSize extends Size {
  case class IconSize(value: String) extends TypedClass(value)

  object IconSize {
    val SMALL    = IconSize(small)
    val NORMAL   = IconSize("")
    val LARGE    = IconSize(large)
    val BIG      = IconSize(big)
    val HUGE     = IconSize(huge)
    val MASSIVE  = IconSize(massive)
  }
}
