package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait ColumnSize {
  case class ColumnSize(value: String) extends TypedClass(value)

  object ColumnSize {
    val NONE     = ColumnSize("")
    val ONE      = ColumnSize(one)
    val TWO      = ColumnSize(two)
    val THREE    = ColumnSize(three)
    val FOUR     = ColumnSize(four)
    val FIVE     = ColumnSize(five)
    val SIX      = ColumnSize(six)
    val SEVEN    = ColumnSize(seven)
    val EIGHT    = ColumnSize(eight)
    val NINE     = ColumnSize(nine)
    val TEN      = ColumnSize(ten)
    val ELEVEN   = ColumnSize(eleven)
    val TWELVE   = ColumnSize(twelve)
    val THIRTEEN = ColumnSize(thirteen)
    val FOURTEEN = ColumnSize(fourteen)
    val FIFTEEN  = ColumnSize(fifteen)
    val SIXTEEN  = ColumnSize(sixteen)
  }
}
