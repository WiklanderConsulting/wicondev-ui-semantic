package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait Size {
  case class Size(value: String) extends TypedClass(value)

  object Size {
    val MINI     = Size(mini)
    val TINY     = Size(tiny)
    val SMALL    = Size(small)
    val DEFAULT  = Size("")
    val MEDIUM   = Size(medium)
    val LARGE    = Size(large)
    val BIG      = Size(big)
    val HUGE     = Size(huge)
    val MASSIVE  = Size(massive)
  }
}
