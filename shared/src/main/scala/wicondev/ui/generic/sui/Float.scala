package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait Float {
  case class Float(value: String*) extends TypedClass(value: _*)

  object Float {
    val NONE     = Float("")
    val LEFT     = Float(left, floated)
    val RIGHT    = Float(right, floated)
  }
}
