package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait MessageType {
  case class MessageType(value: String) extends TypedClass(value)

  object MessageType {
    val SUCCESS = MessageType(success)
    val INFO    = MessageType(info)
    val WARNING = MessageType(warning)
    val ERROR   = MessageType(error)
  }
}
