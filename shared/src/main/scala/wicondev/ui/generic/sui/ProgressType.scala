package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait ProgressType {
  case class ProgressType(value: String) extends TypedClass(value)

  object ProgressType {
    val DEFAULT   = ProgressType("")
    val ACTIVE    = ProgressType(active)
    val SUCCESS   = ProgressType(success)
    val WARNING   = ProgressType(warning)
    val ERROR     = ProgressType(error)
    val DISABLED  = ProgressType(disabled)
  }
}
