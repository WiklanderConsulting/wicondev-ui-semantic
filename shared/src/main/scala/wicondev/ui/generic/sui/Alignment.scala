package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait Alignment {
  case class Alignment(value: String*) extends TypedClass(value: _*)

  object Alignment {
    val NONE     = Alignment("")
    val LEFT     = Alignment(left, aligned)
    val RIGHT    = Alignment(right, aligned)
    val TOP      = Alignment(top, aligned)
    val MIDDLE   = Alignment(middle, aligned)
    val BOTTOM   = Alignment(bottom, aligned)
  }
}
