package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait IconFlip {
  case class IconFlip(value: String*) extends TypedClass(value: _*)

  object IconFlip {
    val NONE         = IconFlip("")
    val HORIZONTALLY = IconFlip(horizontally, flipped)
    val VERTICALLY   = IconFlip(vertically,   flipped)
  }
}
