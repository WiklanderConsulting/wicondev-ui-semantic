package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait MessageSize extends Size {
  case class MessageSize(value: String) extends TypedClass(value)

  object MessageSize {
    val SMALL    = MessageSize(small)
    val NORMAL   = MessageSize("")
    val LARGE    = MessageSize(large)
    val HUGE     = MessageSize(huge)
    val MASSIVE  = MessageSize(massive)
  }
}
