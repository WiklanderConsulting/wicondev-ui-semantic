package wicondev.ui.generic.sui

import classes._
import wicondev.ui.generic.TypedClass

trait PanelType {
  case class PanelType(value: String) extends TypedClass(value)

  object PanelType {
    val DEFAULT    = PanelType("")
    val SUCCESS    = PanelType(success)
    val POSITIVE   = PanelType(positive)
    val INFO       = PanelType(info)
    val WARNING    = PanelType(warning)
    val ERROR      = PanelType(error)
    val NEAGTIVE   = PanelType(negative)
  }
}
