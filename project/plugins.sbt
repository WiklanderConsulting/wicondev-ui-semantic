logLevel := Level.Warn

// See: https://github.com/coursier/coursier/issues/450#issuecomment-302267082
classpathTypes += "maven-plugin"

addSbtPlugin("com.frugalmechanic"   % "fm-sbt-s3-resolver"  % "0.4.0")

addSbtPlugin("org.scala-js"         % "sbt-scalajs"         % "0.6.21")
