import sbt._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

/**
 * Application settings. Configure the build for your application here.
 * You normally don't have to touch the actual build definition after this.
 */
object Settings extends WicondevSettings("UI Semantic") {
  /** The version of your application */
  val version = "0.4.2"

  /**
   * These dependencies are shared between JS and JVM projects
   * the special %%% function selects the correct version for each project
   */
  val sharedDependencies = Def.setting(Seq(
    "se.wicondev" %%% "wicondev-ui-common" % "0.4.0-SNAPSHOT"
  ))

  /** Dependencies only used by the JVM project */
  val jvmDependencies = Def.setting(Seq(
    "org.webjars" % "Semantic-UI" % "2.2.10"
  ))

  /** Dependencies only used by the JS project (note the use of %%% instead of %%) */
  val scalajsDependencies = Def.setting(Seq(
    "be.doeraene" %%% "scalajs-jquery" % "0.9.2"
  ))
  
  /** Dependencies for external JS libs that are bundled into a single .js file according to dependency order */
  val jsDependencies = Def.setting(Seq(
    "org.webjars"       % "jquery"      % "2.1.3" / "2.1.3/jquery.js",
    "org.webjars" % "Semantic-UI" % "2.2.10" / "semantic.js" dependsOn "2.1.3/jquery.js"
  ))
}
