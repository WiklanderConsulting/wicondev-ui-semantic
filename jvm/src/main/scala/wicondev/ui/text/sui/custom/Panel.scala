package wicondev.ui.text.sui.custom

import wicondev.ui.generic.sui.classes
import wicondev.ui.text.sui.all._

trait Panel {
  object Panel {
    def apply(f: Frag*): Frag =
      <.div(s.ui.segment)(f)
    
    def WithHeader(
      panelType: PanelType,
      headerElement: Frag
    )(f: Frag*): Seq[Frag] = Seq(
      <.div(s.ui.top.attached.message.ofType(panelType))(headerElement),
      <.div(s.ui.bottom.attached.segment)(f)
    )

    def WithHeaderAndBody(
      panelType: PanelType,
      headerElement: Frag
    )(f: Frag*): Seq[Frag] = Seq(
      <.div(s.ui.top.attached.message.ofType(panelType))(headerElement),
      f
    )
    
    def WithTitle(
      panelType: PanelType,
      title: String
    )(f: Frag*): Seq[Frag] = Seq(
      <.div(s.ui.top.attached.message.ofType(panelType))(
        <.div(s.header)(title)
      ),
      <.div(s.ui.bottom.attached.segment)(f: _*)
    )

    def WithTitle(
      panelType: PanelType,
      titleElement: Frag
    )(f: Frag*): Seq[Frag] = Seq(
      <.div(s.ui.top.attached.message.ofType(panelType))(titleElement),
      <.div(s.ui.bottom.attached.segment)(f: _*)
    )

    def Title(title: String) = <.h5(s.ui.header)(title)

    def WithHeaderAndFooter(
      panelType: PanelType,
      headerElement: Frag,
      footerElement: Frag
    )(f: Frag*): Seq[Frag] = Seq(
      <.div(s.ui.top.attached.message.ofType(panelType))(headerElement),
      <.div(s.ui.attached.segment)(f),
      <.div(s.ui.bottom.attached.message.ofType(panelType))(footerElement)
    )

    def WithFooter(panelType: PanelType)(f: Frag*)(footerFrag: Frag): Seq[Frag] = Seq(
      <.div(s.ui.top.attached.segment)(f),
      <.div(s.ui.bottom.attached.message.ofType(panelType))(footerFrag)
    )
  }
}
