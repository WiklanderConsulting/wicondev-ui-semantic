package wicondev.ui.text.sui

import wicondev.ui.text.sui.all._
import wicondev.ui.generic.sui.classes

trait Button {
  object Button {
    def apply(
      buttonType: ButtonType              = ButtonType.DEFAULT,
      size:       Size                    = Size.DEFAULT,
      float:      Float                   = Float.NONE,
      buttonTag:  ConcreteHtmlTag[String] = <.div,
      isBlock:    Boolean                 = false
    )(value: Frag*) =
      buttonTag(
        classSet(
          classes.ui          -> true,
          size.asString       -> true,
          buttonType.asString -> true,
          classes.fluid       -> isBlock,
          float.asString      -> true,
          classes.button      -> true
        )
      )(value)

    def Submit(
      buttonType: ButtonType = ButtonType.DEFAULT,
      size:       Size       = Size.DEFAULT,
      float:      Float      = Float.NONE,
      isBlock:    Boolean    = false
    )(value: Frag*) =
      apply(
        buttonType,
        size,
        float,
        <.button(^.tpe := "submit"),
        isBlock
      )(value)


    def Link(
      target: String = "#",
      buttonType: ButtonType = ButtonType.DEFAULT,
      size:       Size       = Size.DEFAULT,
      float:      Float      = Float.NONE,
      isBlock:    Boolean    = false
    )(value: Frag*): Frag =
      apply(
        buttonType,
        size,
        float,
        <.a(^.href := target),
        isBlock
      )(value)
  }
}
