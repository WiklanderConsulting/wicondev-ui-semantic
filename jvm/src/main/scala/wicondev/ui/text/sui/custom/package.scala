package wicondev.ui.text.sui

package object custom {
  trait all  
    extends wicondev.ui.generic.sui.all
    with    Alert
    with    Panel

  object all extends all
}
