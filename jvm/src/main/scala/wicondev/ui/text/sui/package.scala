package wicondev.ui.text

package object sui {
  trait all
    extends wicondev.ui.generic.sui.all
    with    wicondev.ui.text.all
    with    wicondev.ui.text.sui.custom.all

    with    BreadCrumbs
    with    Button
    with    Components
    with    Grid
    with    Icon
    with    Message
    with    Navbar
    with    Progress

  object all extends all {
    import scala.language.implicitConversions
    import wicondev.ui.generic.sui.SuiClassSet

    val ^ = wicondev.ui.text.^
    val < = wicondev.ui.text.<

    implicit def suiClassSet2Modifier(sui: SuiClassSet): Modifier =
      ^.cls := sui.toString

    def s = SuiClassSet()
  }
}
