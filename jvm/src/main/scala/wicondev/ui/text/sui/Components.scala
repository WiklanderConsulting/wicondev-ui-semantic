package wicondev.ui.text.sui

import wicondev.ui.text.sui.all._

trait Components {
  def PageHeader(title: String) =
    <.h2(s.ui.header)(
      title
    )
  
  def PageHeader(title: String, secondary: String) =
    <.h2(s.ui.header)(
      title,
      <.div(s.sub.header)(secondary)
    )
}
