package wicondev.ui.text.sui

import wicondev.ui.generic.sui.classes
import wicondev.ui.text.sui.all._

trait Progress {
  object Progress {
    def apply(
      progressType: ProgressType,
      showValue:    Boolean        = false,
      active:       Boolean        = false,
      label:        Option[String] = None
    ): Frag = <.div(
      classSet1(
        classes.ui,
        classes.active        -> active,
        progressType.asString -> true,
        classes.progress      -> true
      )
    )(
      <.div(s.bar)(
        maybe(showValue)(<.div(s.progress))
      ),
      label map (<.div(s.label)(_))
    )
  }
}
