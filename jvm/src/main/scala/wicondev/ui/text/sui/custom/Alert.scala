package wicondev.ui.text.sui.custom

import wicondev.ui.text.sui.all._

trait Alert {
  object Alert {
    def apply(messageType: MessageType = MessageType.INFO)(f: Frag*): Frag =
      <.div(
        s.ui.withClass("custom-alert"),
        ^.role:="alert"
      )(
        Message(
          messageType = messageType,
          messageSize = MessageSize.LARGE
        )(f: _*)
      )
  }
}
