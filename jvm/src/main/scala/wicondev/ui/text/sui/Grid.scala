package wicondev.ui.text.sui

import wicondev.ui.text.sui.all._

trait Grid {
  object Grid {
    def apply(f: Frag*): Frag =
      <.div(s.ui.grid)(f)
    
    def Centered(f: Frag*): Frag =
      <.div(s.ui.centered.grid)(f)
  }

  def Row(f: Frag*): Frag =
    <.div(s.row)(f)
  
  def Row(customClass: String)(f: Frag*): Frag =
    <.div(s.row.withClass(customClass))(f)

  def Column(f: Frag*): Frag = Column(ColumnSize.NONE)(f: _*)
  
  def Column(
    size:       ColumnSize  = ColumnSize.NONE,
    hAlignment: Alignment   = Alignment.NONE,
    vAlignment: Alignment   = Alignment.NONE,
    float: Float            = Float.NONE
  )(f: Frag*): Frag =
    <.div(
      classSetS(
        hAlignment.asString,
        vAlignment.asString,
        float.asString,
        if(size isNot ColumnSize.NONE) s"$size ${s.wide}" else "",
        s.column
      )
    )(f)
}
