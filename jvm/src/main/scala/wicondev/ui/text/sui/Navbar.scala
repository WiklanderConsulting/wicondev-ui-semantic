package wicondev.ui.text.sui

import wicondev.ui.generic.sui.classes
import wicondev.ui.text.sui.all._

trait Navbar {
  object Menu {
    def Navbar(f: Frag*) =
      <.div(
        s.ui.large.fixed.menu,
        ^.role:="navigation"
      )(f: _*)

    def Right(f: Frag*):Frag = <.div(s.right.menu)(f)
      
    def Item(
      itemHref: String,
      caption:  String
    ):Frag = Item(
      itemHref = itemHref,
      caption  = caption,
      iconType = None,
      isActive = false)

    def Item(
      itemHref: String,
      caption:  String,
      isActive: Boolean
    ):Frag = Item(
      itemHref = itemHref,
      caption  = caption,
      iconType = None,
      isActive = isActive)

    def Item(
      itemHref: String,
      caption:  String,
      iconType: IconType
    ):Frag = Item(
      itemHref = itemHref,
      caption  = caption,
      iconType = Some(iconType),
      isActive = false)


    def Item(
      itemHref: String,
      caption:  String,
      iconType: IconType,
      isActive: Boolean
    ):Frag = Item(
      itemHref = itemHref,
      caption  = caption,
      iconType = Some(iconType),
      isActive = isActive)

    private def Item(
      itemHref: String,
      caption:  String,
      iconType: Option[IconType],
      isActive: Boolean
    ):Frag = <.a(
      classSet1(
        classes.item,
        classes.active -> isActive
      ),
      ^.href := itemHref
    )(
      iconType map (Icon(_)),
      caption
    )

    def Header(f: Frag*):Frag = <.div(s.header.item)(f)

    def DropDown(
      title: String
    )(items: Frag*): Frag =
      DropDown(title, None)(items: _*)

    def DropDown(
      title: String,
      iconType: IconType
    )(items: Frag*): Frag =
      DropDown(title, Some(iconType))(items: _*)

    private def DropDown(
      title: String,
      iconType: Option[IconType]
    )(items: Frag*): Frag =
      <.div(s.ui.dropdown.link.item)(
        iconType map (Icon(_)),
        title,
        Icon(IconType.DROPDOWN),
        <.div(s.menu)(items)
      )
  }
}
