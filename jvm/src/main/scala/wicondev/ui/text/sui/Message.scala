package wicondev.ui.text.sui

import wicondev.ui.text.sui.all._

trait Message {
  object Message {
    def apply(
      messageType: MessageType = MessageType.INFO,
      messageSize: MessageSize = MessageSize.NORMAL
    )(f: Frag*): Frag =
      makeMessage(
        messageType = messageType,
        messageSize = messageSize,
        dismissable = false
      )(f: _*)

    def Dismissable(
      messageType: MessageType = MessageType.INFO,
      messageSize: MessageSize = MessageSize.NORMAL
    )(f: Frag*): Frag =
      makeMessage(
        messageType = messageType,
        messageSize = messageSize,
        dismissable = true
      )(f: _*)

    private def makeMessage(
      messageType: MessageType,
      messageSize: MessageSize,
      dismissable: Boolean
    )(f: Frag*): Frag = 
      <.div(
        s.ui.message.withSize(messageSize).ofType(messageType),
        ^.role:="alert"
      )(
        maybe(dismissable)(
          Icon(IconType.CLOSE)
        ),
        f
      )
  }
}
