package wicondev.ui.text.sui

import wicondev.ui.text.sui.all._
import wicondev.ui.generic.sui.classes

trait Icon {
  object Icon {
    def apply(
      iconType:       IconType,
      iconSize:       IconSize     = IconSize.NORMAL,
      iconFlip:       IconFlip     = IconFlip.NONE,
      iconRotatation: IconRotation = IconRotation.NONE,
      iconTag:        ConcreteHtmlTag[String] = <.i
    ): Frag =
      iconTag(
        typedClassSet1(
          classes.icon,
          iconType,
          iconFlip,
          iconRotatation,
          iconSize
        )
        // Icon need *some* content to render
      )(" ")

    def linked(
        iconType:       IconType,
        iconSize:       IconSize     = IconSize.NORMAL,
        iconFlip:       IconFlip     = IconFlip.NONE,
        iconRotatation: IconRotation = IconRotation.NONE,
        iconTag:        ConcreteHtmlTag[String] = <.i
      ):Frag =
      iconTag(
        s.icon.link,
        typedClassSet(
          iconType,
          iconFlip,
          iconRotatation,
          iconSize
        )
        // Icon need *some* content to render
      )(" ")
  }
}
